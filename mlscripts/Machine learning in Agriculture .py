#!/usr/bin/env python
# coding: utf-8

# # Machine learning in Agriculture
#       
# Using machine learning implications on a modern agriculture practise.
# 
# #### Dataset information
# 
# The crop recommendation dataset is made up of parameters for appropriate crops to grow in a particular farm. The data was built by augmenting datasets of rainfall, climate and fertilizer data available in India. 
#       
#  #### Attribute information:
#  
#     N - ratio of Nitrogen content in soil
#     P - ratio of Phosphorous content in soil
#     K - ratio of Potassium content in soil
#     temperature - temperature in degree Celsius
#     ph - ph value of the soil
#     rainfall - rainfall in mm
#     
# #### Data taken from:
# 
#    https://www.kaggle.com/atharvaingle/crop-recommendation-dataset
#    
# #### Goal:
# 
# Give the most suitable parameter recommendation for the crop of choice to grow in an Aeroponic farm setting.
#    
# Selime Keskin 14164250 University of Hertfordshire 

# ### Loading libraries
# 
# Load the main libraries/methods that will be used throughout 

# In[5]:


import pandas as pd

import numpy as np

import seaborn as sns

from numpy import mean

from numpy import absolute

from numpy import std

import sklearn.metrics as metrics

import matplotlib.pyplot as plt

get_ipython().run_line_magic('matplotlib', 'inline')

from sklearn.model_selection import train_test_split

from sklearn.preprocessing import LabelEncoder

from sklearn.model_selection import RepeatedKFold

from sklearn.model_selection import cross_val_score


# #### Load data 

# In[6]:


# Read in the csv file and display some of the basic info
pd.read_csv ("Crop_recommendation.csv")
crop_df = pd.read_csv ("Crop_recommendation.csv")


# In[7]:


crop_df.head()


# In[8]:


crop_df.describe()


# #### drop irrelevant columns

# In[9]:


cols_to_drop = ['humidity']
crop_df = crop_df.drop(cols_to_drop, axis = 1)


# In[10]:


crop_df.head()


# #### check for missing values 

# In[11]:


sns.heatmap(crop_df.isnull())


# #### Mean average for crop parameters

# In[12]:


crop = crop_df.groupby(['label']).mean()
print(crop)


# In[13]:


df = crop.add_suffix('').reset_index()
df.head()


# #### Data visualisation

# In[14]:


# Create a basic bar chart for Ph levels according to crop selection
plt.bar("label", "ph", data = crop_df , color = "blue")
plt.xlabel("Crop selection")
plt.xticks(rotation = 90)
plt.ylabel("PH value of the soil ")
plt.title("Average Ph value of soil according to crop selection")
plt.show()


# In[15]:


# Create a basic bar chart for rainfall according to crop selection
plt.bar("label", "rainfall", data = crop_df, color = "blue")
plt.xlabel("Crop selection")
plt.xticks(rotation = 90)
plt.ylabel("Rainfall in mm")
plt.title("Average rainfall in mm according to crop selection")
plt.show()


# In[16]:


# Create a basic bar chart for temperature levels according to crop selection
plt.bar("label", "temperature", data = crop_df, color = "blue")
plt.xlabel("Crop selection")
plt.xticks(rotation = 90)
plt.ylabel("Temperature in degree Celsius (°C)")
plt.title("Average temperature according to crop selection")
plt.show()


# In[17]:


# Filter the columns for crop nutrients and groups for stacked bars
soil = crop_df[['N', 'P', 'K', 'label']]
soil.head()
selection_group = soil.groupby('label')
selection_group.size()
category_group = soil.groupby(['label']).mean()
category_group.head()


# In[18]:


# Plot and show the stacked bar chart
stack_bar_plot = category_group.plot(kind='bar', stacked=True, title="Ratio of Crucial Soil Nutrients", figsize=(9, 7))
stack_bar_plot.set_xlabel("Crop selection")
stack_bar_plot.set_ylabel("Ratio of Potassium, Phosphorous and Nitrogen content in soil")
stack_bar_plot.legend(["Nitrogen","Phosphorous","Potassium"], loc=9,ncol=4)


# #### From label to numerical

# In[19]:


le = LabelEncoder()


# In[20]:


label = le.fit_transform(df.label)
df['label'] = label
df.head()


# #### Partition of the data into training and testing data sets

# In[21]:


X = df.iloc[:, [0]].values
y = df.iloc[:, [1,2,3,4,5, 6]].values


# In[22]:


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=100)


# In[23]:


print ('Shape of X:', X.shape)
print ('Shape of y:', y.shape)
print ('Shape of X_train:', X_train.shape)
print ('Shape of y_train:', y_train.shape)
print ('Shape of X_test:', X_test.shape)
print ('Shape of y_test:', y_test.shape)


# #### Decision Tree Regressor

# In[24]:


#Initializing the Model and setting parameters if any

from sklearn.tree import DecisionTreeRegressor

dtr =  DecisionTreeRegressor(random_state = 0)


# In[25]:


#fitting the training data to the model

dtr.fit(X_train, y_train)


# In[26]:


#output decision tree regressor prediction

y_pred = dtr.predict(X_test)
print('Sample predictions are:\n', y_pred[:5].flatten())


# In[27]:


# plotting the best-fit line

plt.scatter(y_test, y_pred)
plt.plot([y.min(), y.max()], [y.min(), y.max()], 'k--', lw=4)
plt.xlabel("Actual")
plt.ylabel("Predicted")
plt.title("Actual vs Predicted")

plt.show()


# In[28]:


#outputs the training and testing scores

print("Train Accuracy:",dtr.score(X_train, y_train))
print("Test Accuracy:",dtr.score(X_test, y_test))


# In[29]:


#Repeated K-Fold Cross Validation

cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)

n_scores = cross_val_score(dtr, X, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)

n_scores = absolute(n_scores)

print('MAE with Cross-Validation: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))


# In[30]:


#Evaluate the regressor

mae = metrics.mean_absolute_error(y_test, y_pred)
mse = metrics.mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mse) # or mse**(0.5)  
r2 = metrics.r2_score(y_test, y_pred)

print("Results of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)


# #### Linear Regression

# In[31]:


#Initializing the Model and setting parameters if any

from sklearn.linear_model import LinearRegression

lr = LinearRegression()


# In[32]:


#fitting the training data to the model

lr.fit(X_train, y_train)


# In[33]:


#output decision tree regressor prediction

y_pred = lr.predict(X_test)
print('Sample predictions are:\n', y_pred[:5].flatten())


# In[34]:


# plotting the best-fit line

plt.scatter(y_test, y_pred)
plt.plot([y.min(), y.max()], [y.min(), y.max()], 'k--', lw=4)
plt.xlabel("Actual")
plt.ylabel("Predicted")
plt.title("Actual vs Predicted")

plt.show()


# In[35]:


#outputs the training and testing scores

print("Train Accuracy:",lr.score(X_train, y_train))
print("Test Accuracy:",lr.score(X_test, y_test))


# In[36]:


#Repeated K-Fold Cross Validation

cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)

n_scores = cross_val_score(lr, X, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)

n_scores = absolute(n_scores)

print('MAE with Cross-Validation: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))


# In[37]:


#Evaluate the regressor

mae = metrics.mean_absolute_error(y_test, y_pred)
mse = metrics.mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mse) # or mse**(0.5)  
r2 = metrics.r2_score(y_test, y_pred)

print("Results of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)


# #### KNeighbors Regressor

# In[38]:


#Initializing the Model and setting parameters if any

from sklearn.neighbors import KNeighborsRegressor

knn = KNeighborsRegressor()


# In[39]:


#fitting the training data to the model

knn.fit(X_train, y_train)


# In[40]:


#output decision tree regressor prediction

y_pred = knn.predict(X_test)
print('Sample predictions are:\n', y_pred[:5].flatten())


# In[41]:


# plotting the best-fit line

plt.scatter(y_test, y_pred)
plt.plot([y.min(), y.max()], [y.min(), y.max()], 'k--', lw=4)
plt.xlabel("Actual")
plt.ylabel("Predicted")
plt.title("Actual vs Predicted")

plt.show()


# In[42]:


#outputs the training and testing scores

print("Train Accuracy:", knn.score(X_train, y_train))
print("Test Accuracy:", knn.score(X_test, y_test))


# In[43]:


#Repeated K-Fold Cross Validation

cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)

n_scores = cross_val_score(knn, X, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)

n_scores = absolute(n_scores)

print('MAE with Cross-Validation: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))


# In[44]:


#Evaluate the regressor

mae = metrics.mean_absolute_error(y_test, y_pred)
mse = metrics.mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mse) # or mse**(0.5)  
r2 = metrics.r2_score(y_test, y_pred)

print("Results of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)


# #### support vector machine regression

# In[45]:


#Initializing the Model and setting parameters if any

from sklearn.svm import LinearSVR
from sklearn.multioutput import RegressorChain

svm = LinearSVR()


# In[46]:


#fitting the training data to the model

mor = RegressorChain(svm, order=[0,1,2,3,4,5])
mor.fit(X_train, y_train)


# In[47]:


#output decision tree regressor prediction

y_pred = mor.predict(X_test)
print('Sample predictions are:\n', y_pred[:5].flatten())


# In[48]:


# plotting the best-fit line

plt.scatter(y_test, y_pred)
plt.plot([y.min(), y.max()], [y.min(), y.max()], 'k--', lw=4)
plt.xlabel("Actual")
plt.ylabel("Predicted")
plt.title("Actual vs Predicted")

plt.show()


# In[49]:


#outputs the training and testing scores

print("Train Accuracy:", mor.score(X_train, y_train))
print("Test Accuracy:", mor.score(X_test, y_test))


# In[50]:


#Repeated K-Fold Cross Validation

cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)

n_scores = cross_val_score(mor, X, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)

n_scores = absolute(n_scores)

print('MAE with Cross-Validation: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))


# In[51]:


#Evaluate the regressor

mae = metrics.mean_absolute_error(y_test, y_pred)
mse = metrics.mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mse) # or mse**(0.5)  
r2 = metrics.r2_score(y_test, y_pred)

print("Results of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)


# In[ ]:




